# Docker container packaging Ansible and other unit test utilities


## How to Build the image
- Build:
```
docker build -t "ansible_aos:latest" .
```
- Check image:
```
docker images | grep ansible
```
- Check Ansible command:
```
$ docker run --rm -it ansible_aos
ansible-playbook 2.9.2
  config file = None
  configured module search path = [u'/ansible/library']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible-playbook
  python version = 2.7.16 (default, May  6 2019, 19:28:45) [GCC 8.3.0] 
```
